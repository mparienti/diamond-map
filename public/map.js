import { Color, Gradient, generate } from "./lib/classes.js";

const noisyimage = generate(10);

const gradient = new Gradient();

gradient.addPoint(noisyimage.min(), new Color('000')); //
gradient.addPoint(noisyimage.min()*0.5, new Color('00008b')); //
gradient.addPoint(noisyimage.min()*0.4, new Color('0000ee'));//
gradient.addPoint(noisyimage.min()*0.35, new Color('0000ff'));
//gradient.addPoint(noisyimage.min()*0.30, new Color('009acd'));
//gradient.addPoint(noisyimage.min()*0.20, new Color('189acd'));
//gradient.addPoint(noisyimage.min()*0.10, new Color('419ae1'));
gradient.addPoint(noisyimage.min()*0.005, new Color('66E6FF')); //
gradient.addPoint(0, new Color('fff')); // ecume
gradient.addPoint(noisyimage.max()*.01, new Color('ffefdb')); // sand
gradient.addPoint(noisyimage.max()/10, new Color('228b22')); // foret
gradient.addPoint(noisyimage.max()*.5, new Color('006400')); // foret
gradient.addPoint(noisyimage.max()/20, new Color('bdb76b')); // foret
gradient.addPoint(noisyimage.max()*.65, new Color('#8b8878')); // montain
gradient.addPoint(noisyimage.max()*.9, new Color('#eef')); // snow
gradient.addPoint(noisyimage.max(), new Color('fff')); // snow


// Build array
const rawdata = noisyimage.data;
const width = noisyimage.width;
const height = noisyimage.height;
const sarray = [];

for (let i = 0; i < rawdata.length; i += 1) {
  let color = gradient.getColorValue(rawdata[i]);
  sarray[4*i] = color._red;
  sarray[4*i+1] = color._green;
  sarray[4*i+2] = color._blue;
  sarray[4*i+3] = 255;
}

const u8array = new Uint8ClampedArray(sarray);

// Build image
const image = new ImageData(u8array, width, height);

// Render Image
const canvas = document.getElementById('canvas');
const ctx = canvas.getContext('2d');

canvas.width = width;
canvas.height = height;
ctx.putImageData(image, 0, 0);

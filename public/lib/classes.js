import { Color } from "./color.js";
import { Gradient, gradientFactory } from "./gradient.js";
import { generate } from "./generate.js";

export { Color, Gradient, gradientFactory, generate };

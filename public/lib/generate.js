
class NoiseImage {
  constructor(width, height) {
    this.width = width;
    this.height = height;
    this.data = [];
    this._max = Number.NEGATIVE_INFINITY;
    this._min = Number.POSITIVE_INFINITY;
  }

  set(x, y, value) {
    this.data[y + x * this.width] = value;
    if (this._min > value) {
      this._min = value;
    }
    if (this._max < value) {
      this._max = value;
    }
  }

  get(x, y) {
    return this.data[y + x * this.width];
  }

  max() {
    return this._max;
  }

  min() {
    return this._min;
  }
}


function getRandomIntInclusive(min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min + 1)) + min; //The maximum is inclusive and the minimum is inclusive 
}

function getRandomArbitrary(min, max) {
  return Math.random() * (max - min ) + min;
}


/*
  Will generate a 2 dimension array of the size 2^n + 1, filled with random values
  between [-(2^n + 1), (2^n + 1)]

  Diamant-carré

  https://github.com/spbooth/scriptmountains/blob/master/Mountains.js

  https://fr.wikipedia.org/wiki/Algorithme_Diamant-Carr%C3%A9
*/
const generate = function(n = 16) {

  const dim = 2**n + 1;
  const img = new NoiseImage(dim,dim);
  const randomFunction = getRandomIntInclusive;

  // Init corner
  img.set(0, 0, randomFunction(dim*-1, dim));
  img.set(0, dim-1, randomFunction(dim*-1, dim));
  img.set(dim-1, 0, randomFunction(dim*-1, dim));
  img.set(dim-1, dim-1, randomFunction(dim*-1, dim));


  let size = dim - 1;

  while (size > 1) {
    let step = size / 2;
    // Diamant
    for (let x = step; x < dim; x=x+size) {
      for (let y = step; y < dim; y=y+size) {
        let avg = (img.get(x - step, y - step) + img.get(x - step, y + step) + img.get(x + step, y + step) + img.get(x + step, y - step)) / 4;
        img.set(x,y, Math.floor(avg) + randomFunction(step*-1, step));
      }
    }

    let pitch = 0;

    for (let x = 0; x < dim; x=x+step) {
      pitch = (pitch ==0)? step : 0;
      for (let y = pitch; y < dim; y=y+size) {
        let sum = 0, n = 0;
        if (x >= step) {
          sum += img.get(x - step,y);
          n = n+1;
        }
        if (x+ step < dim) {
          sum += img.get(x + step,y);
          n = n+1;
        }
        if (y >= step) {
          sum += img.get(x, y - step);
          n = n+1;
        }
        if (y+ step < dim) {
          sum += img.get(x,y + step);
          n = n+1;
        }
        img.set(x, y, Math.floor(sum/n) + randomFunction(step*-1, step));
      }
    }
    size = step;
  }
  return img;
};

export { generate }

class Color {
  /*
    waiting for https://github.com/tc39/proposal-class-fields
    _red = 0;
    _green = 0;
    _blue = 0;
  */
  constructor(value = '000') {
    value = value.replace(/^#/, '');
    if(value.length == 3) {
      value = value.replace(/([0-9a-f])/gi, '$1$1');
    }
    if (value.length != 6 ||
        ! /^[0-9a-f]{6}$/i.test(value)
       ) {
      throw new Error('Value invalid');
    }
    this._red = parseInt(value.slice(0,2), 16);
    this._green = parseInt(value.slice(2,4), 16);
    this._blue = parseInt(value.slice(4,6), 16);
  }
}

export { Color }

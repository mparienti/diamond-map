import { Color } from "./color.js";

class Gradient {
  constructor(){
    this._level = [];
    this._colors = {};
    this._nblevel = 0;
  }

  addPoint(level, color) {
    if (!isFinite(level)) {
      throw new Error('Level arg should be a number');
    }
    if (!color instanceof Color) {
      throw new Error('Color arg should be an instance of Color class');
    }
    const index = parseFloat(level);
    this._level.push(index);
    this._colors[index] = color;
    this._level.sort((a,b) => { const fa = parseFloat(a), fb = parseFloat(b); return (fa<fb)? -1 : ((fa>fb)? 1 : 0)  });//does not work
    this._nblevel += 1;
  }

  getColorValue(point) {
    if (!isFinite(point)) {
      throw new Error('Level arg should be a number');
    }
    if (point < this._level[0]) {
      return this._colors[this._level[0]];
    }
    if (point > this._level[this._nblevel-1]) {
      return this._colors[this._level[this._nblevel-1]];
    }
    for (let i = 0; i < this._level.length-1; i++) {
      if ( point >= this._level[i] &&
           point <= this._level[i+1] ) {
        return this.getInsideColor(point,i);
      }
    }
    console.log('error with' + point);
  }

  getInsideColor(value, level_inf) {
    const from_color = this._colors[this._level[level_inf]];
    const to_color = this._colors[this._level[level_inf + 1]];
    const from_value = this._level[level_inf];
    const to_value = this._level[level_inf + 1];
    const pourcent = 100 * (value - from_value) / (to_value - from_value);
    //return from_color;
    return this.addPourcent(pourcent, from_color, to_color);

  }

  addPourcent(pourcent, from_color, to_color) {
    const new_color = new Color();
    const components = ['_red', '_green', '_blue'];
    components.forEach((component) => {
      new_color[component] = from_color[component] + pourcent * (to_color[component] - from_color[component]) /100;
    }
                      );
    return new_color;
  }
}

const gradientFactory = function(image) {
  const gradient = new Gradient();

  gradient.addPoint(image.min(), new Color('000')); //
  gradient.addPoint(image.min()*0.5, new Color('00008b')); //
  gradient.addPoint(image.min()*0.4, new Color('0000ee'));//
  gradient.addPoint(image.min()*0.35, new Color('0000ff'));
  gradient.addPoint(image.min()*0.005, new Color('66E6FF')); //
  gradient.addPoint(0, new Color('fff')); // ecume
  gradient.addPoint(image.max()*.01, new Color('ffefdb')); // sand
  gradient.addPoint(image.max()/10, new Color('228b22')); // foret
  gradient.addPoint(image.max()*.5, new Color('006400')); // foret
  gradient.addPoint(image.max()/20, new Color('bdb76b')); // foret
  gradient.addPoint(image.max()*.65, new Color('#8b8878')); // montain
  gradient.addPoint(image.max()*.9, new Color('#eef')); // snow
  gradient.addPoint(image.max(), new Color('fff')); // snow

  return gradient;
};

export { Gradient, gradientFactory }

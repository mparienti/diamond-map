
// Import
import * as THREE from "./vendor/three.module.js";
import { Color, Gradient, gradientFactory, generate } from "./lib/classes.js";


// Image constante
const N = 9;
const world_width = Math.pow(2, N) + 1;
const world_height = Math.pow(2, N) + 1;

// Three Constant
const plane_size = 70;
const max_height = 25.0;
//const camera_postion = {x: 0, z: 20, y: -15};

const noisyimage = generate(N);
const gradient = gradientFactory(noisyimage);


function display2DMap(image, world_width, world_height){
  // Render Image
  const canvas = document.getElementById('canvas-2d');
  const ctx = canvas.getContext('2d');

  canvas.width = world_height;
  canvas.height = world_width;
  ctx.putImageData(image, 0, 0);

}

const scene = new THREE.Scene();
const canvas_3d = document.getElementById('canvas-3d');
const renderer = new THREE.WebGLRenderer( { canvas: canvas_3d } );
//renderer.setViewport(0, 0, world_width, world_height);
// setSize resize the canvas!
renderer.setSize(world_width, world_height);

const camera = new THREE.PerspectiveCamera( 60, world_width / world_height, 0.1, 100000 );


//const renderer = new THREE.WebGLRenderer();
scene.background = new THREE.Color( 0xFFFFFF );

const see_geometry = new THREE.PlaneGeometry( plane_size, plane_size, world_width -1, world_height -1);
const flatplan_geometry = new THREE.PlaneGeometry( plane_size, plane_size, world_width -1, world_height -1);
const transplan_geometry = new THREE.PlaneGeometry( plane_size, plane_size, world_width -1, world_height -1);
const flatvertices = flatplan_geometry.attributes.position.array;
const transvertices = transplan_geometry.attributes.position.array;
const see_vertices = see_geometry.attributes.position.array;

for ( let i = 2, j=0, l = flatvertices.length; i < l; i += 3, j++) {
  const position = j;
  const current_value = noisyimage.data[position];
  flatvertices[i] =  current_value > 0 ? (max_height* current_value/ world_width ): 0;
  transvertices[i] =  current_value > 0 ? (max_height* current_value/ world_width ): 0;
  see_vertices[i] =  (max_height* current_value / world_width );
}

const rawdata = noisyimage.data;
const flat_sarray = [];
const trans_sarray = [];

for (let i = 0; i < rawdata.length; i += 1) {
  const position = (i %(world_width)) + (world_width - 1 - Math.floor(i/(world_height)))*(world_height);

  let color = gradient.getColorValue(rawdata[position]);
  trans_sarray[4*i] = color._red;
  trans_sarray[4*i+1] = color._green;
  trans_sarray[4*i+2] = color._blue;
  trans_sarray[4*i+3] = rawdata[position] > 0 ? 255 : 127;

  flat_sarray[4*i] = color._red;
  flat_sarray[4*i+1] = color._green;
  flat_sarray[4*i+2] = color._blue;
  flat_sarray[4*i+3] = 255;
}

const trans_u8array = new Uint8ClampedArray(trans_sarray);
const flat_u8array = new Uint8ClampedArray(flat_sarray);


// Build image
const flatplan_image = new ImageData(flat_u8array, world_width, world_height);
const transplan_image = new ImageData(trans_u8array, world_width, world_height);

display2DMap(flatplan_image, world_width, world_width);

const flatplan_texture = new THREE.DataTexture(flatplan_image, world_width, world_height);
const transplan_texture = new THREE.DataTexture(transplan_image, world_width, world_height);

const flatplan_material = new THREE.MeshBasicMaterial(
  {
    map: flatplan_texture,
    opacity: 1,
    transparent: true,
  }
);
const transplan_material = new THREE.MeshBasicMaterial(
  {
    map: transplan_texture,
    opacity: 1,
    transparent: true,
  }
);
const see_material = new THREE.MeshBasicMaterial(
  {
    map: flatplan_texture,
  }
);

const flatplan = new THREE.Mesh( flatplan_geometry, flatplan_material );
const transplan = new THREE.Mesh( transplan_geometry, transplan_material );
const see = new THREE.Mesh( see_geometry, see_material );

see_geometry.rotateZ( - Math.PI / 2);
flatplan_geometry.rotateZ( - Math.PI / 2 );
transplan_geometry.rotateZ( - Math.PI / 2 );


scene.add( transplan );
scene.add( see );
let state = 1;
function toogle() {
  if (state == 1) {
    state = 0;
    scene.add( flatplan );
    scene.remove( transplan );
  } else {
    state = 1;
    scene.add( transplan );
    scene.remove( flatplan );
  }
}

camera.position.x = 0;
camera.position.y = -35;
camera.position.z = 30;

camera.lookAt(0,0,0);

const delta = 0.01;

const animate = function () {
  requestAnimationFrame( animate );

  flatplan.rotation.z += delta;
  transplan.rotation.z += delta;
  see.rotation.z += delta;
  if ( (see.rotation.z % (2*Math.PI)) < delta) {
    toogle();
  }

  renderer.render( scene, camera );
};



animate();


import * as THREE from "./vendor/three.module.js";
import { Color, Gradient, generate } from "./lib/classes.js";

const N = 9;
const noisyimage = generate(N);
const world_width = Math.pow(2, N) + 1;
const world_height = Math.pow(2, N) + 1;
const plane_size = 70;
const max_height = 25.0;
//const camera_postion = {x: 0, z: 20, y: -15};

const scene = new THREE.Scene();
const camera = new THREE.PerspectiveCamera( 60, window.innerWidth / window.innerHeight, 0.1, 100000 );

const renderer = new THREE.WebGLRenderer();
renderer.setSize( window.innerWidth, window.innerHeight );
document.body.appendChild( renderer.domElement );

scene.background = new THREE.Color( 0xFFFFFF );

const see_geometry = new THREE.PlaneGeometry( plane_size, plane_size, world_width -1, world_height -1);
const plan_geometry = new THREE.PlaneGeometry( plane_size, plane_size, world_width -1, world_height -1);
const vertices = plan_geometry.attributes.position.array;
const see_vertices = see_geometry.attributes.position.array;

for ( let i = 2, j=0, l = vertices.length; i < l; i += 3, j++) {
  const position = j;
  const current_value = noisyimage.data[position];
  vertices[i] =  current_value > 0 ? (max_height* current_value/ world_width ): 0;
  see_vertices[i] =  (max_height* current_value/ world_width );
}

const rawdata = noisyimage.data;
const sarray = [];


const gradient = new Gradient();

gradient.addPoint(noisyimage.min(), new Color('000')); //
gradient.addPoint(noisyimage.min()*0.5, new Color('00008b')); //
gradient.addPoint(noisyimage.min()*0.4, new Color('0000ee'));//
gradient.addPoint(noisyimage.min()*0.35, new Color('0000ff'));
gradient.addPoint(noisyimage.min()*0.005, new Color('66E6FF')); //
gradient.addPoint(0, new Color('fff')); // ecume
gradient.addPoint(noisyimage.max()*.01, new Color('ffefdb')); // sand
gradient.addPoint(noisyimage.max()/10, new Color('228b22')); // foret
gradient.addPoint(noisyimage.max()*.5, new Color('006400')); // foret
gradient.addPoint(noisyimage.max()/20, new Color('bdb76b')); // foret
gradient.addPoint(noisyimage.max()*.65, new Color('#8b8878')); // montain
gradient.addPoint(noisyimage.max()*.9, new Color('#eef')); // snow
gradient.addPoint(noisyimage.max(), new Color('fff')); // snow

for (let i = 0; i < rawdata.length; i += 1) {
  const position = (i %(world_width)) + (world_width - 1 - Math.floor(i/(world_height)))*(world_height);

  let color = gradient.getColorValue(rawdata[position]);
  sarray[4*i] = color._red;
  sarray[4*i+1] = color._green;
  sarray[4*i+2] = color._blue;
  sarray[4*i+3] = rawdata[position] > 0 ? 255 : 127;
}

const u8array = new Uint8ClampedArray(sarray);


// Build image
const plan_image = new ImageData(u8array, world_width, world_height);
const plan_texture = new THREE.DataTexture(plan_image, world_width, world_height);

const plan_material = new THREE.MeshBasicMaterial(
  {
    map: plan_texture,
    opacity: 1,
    transparent: true,
  }
);
const see_material = new THREE.MeshBasicMaterial(
  {
    map: plan_texture,
  }
);
plan_material.needsUpdate = true;


const plan = new THREE.Mesh( plan_geometry, plan_material );
const see = new THREE.Mesh( see_geometry, see_material );

see_geometry.rotateZ(- Math.PI / 2);
plan_geometry.rotateZ( - Math.PI / 2 );

scene.add( plan );
scene.add( see );

camera.position.x = 0;
camera.position.y = -35;
camera.position.z = 20;

camera.lookAt(0,0,0);

const animate = function () {
  requestAnimationFrame( animate );

  plan.rotation.z += 0.01;
  see.rotation.z += 0.01;

  renderer.render( scene, camera );
};

animate();






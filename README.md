# Diamond Map

This is procedural map generator based on [Diamond Square algorithm](https://en.wikipedia.org/wiki/Diamond-square_algorithm).

![Map generated](./diamond-map.jpeg)

You can enjoy:
* [a continuously new generated diamond map](https://mparienti.gitlab.io/diamond-map/)
* [a 3D version of the diamond map](https://mparienti.gitlab.io/diamond-map/three.html)
